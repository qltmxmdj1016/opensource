///////////////Parameters/////////////////
var cseUrl = "http://127.0.0.1:7579";
var aeId = "S"+"notiapplication"; 
var aeName = "notiapplication";
var aeIp = "10.0.1.19"; //설치되어있는 컴퓨터 ip
var aePort = 15926; //이건 무엇일까요,,,
var sub_Container = "/Mobius/control/JoyStick"; //니 lumi 변화에 따라서 lamp가 변동됨
var threshold = 300; //어떤 기준값에 의해서 조절할지
var count = 0;
var arr_container = [];
arr_container[count] = {};
arr_container[count++].path = "/Mobius/lamp/COMMAND"; //여러개 lamp 제어할 수도 있기 때문에 array로
//이거 지은언니 설명 좀 보쟈
// arr_container[count] = {};
// arr_container[count++].path = "/Mobius/lamp1/COMMAND";
//////////////////////////////////////////

var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();

app.use(bodyParser.json());
app.listen(aePort, function () {
   console.log("AE Notification listening on: "+aeIp+":"+aePort);
});

//var cnt_value = 0; //lamp_value
app.post("/menu", function(req, res){ //메뉴 선택 시 등록
   var req_body = req.body["m2m:sgn"].nev.rep["m2m:cin"];
   if(req_body != undefined){
      console.log("\n[NOTIFICATION]")
      console.log(req_body);
      var content = req_body.con; // Name of Menu
      var cinA=GetCin("http://10.0.1.19:7579","menu","Americano");
      var cinL=GetCin("http://10.0.1.19:7579","menu","Latte");
      var cinT=GetCin("http://10.0.1.19:7579","menu","Tea");
      if(content == 1){ //A일 경우 cin->A
         createContenInstance("http://10.0.1.19:7579","menu","menu", "Americano");//cnt_value
         //createContenInstance(AEURL,AE,CNT,value)
         //메뉴의 cin 생성 . 아메리카노
      }
      else if(content == 2){ //cinL->'L'
         createContenInstance("http://10.0.1.19:7579","menu","menu", "Latte");
         //메뉴의 cin 생성 . 라떼
      }
      else if(content == 3){
         createContenInstance("http://10.0.1.19:7579","menu","menu", "Tea");
         //메뉴의 cin 생성 . 티
      }
      res.sendStatus(200);
   }
});

app.post("/control", function(req, res){
   var req_body = req.body["m2m:sgn"].nev.rep["m2m:cin"];
   if(req_body != undefined){
      console.log("\n[NOTIFICATION]")
      console.log(req_body);
      //var cin = GetCin("http://10.0.1.19:7579","control","JoyChar"); ///
      var content = req_body.con; 
      //console.log("\ncontent: ",content);
      //console.log("\ncin: ",cin);
      if(content == 1){ //Left일 경우
         createContenInstance("http://10.0.1.19:7579","control","JoyChar", "L");
         //createContenInstance(AEURL,AE,CNT,value)
         //vuejs에서 화면조작 -> L
      }
      else if(content == 2){ //Right 일 경우
        // createContenInstance("http://10.0.1.19:7579","menu","JoyChar", "Right");
        createContenInstance("http://10.0.1.19:7579","control","JoyChar", "R");
         //vuejs에서 화면조작 -> R
      }
      res.sendStatus(200);
   }
});
/*
app.post("/ring", function(req, res){ //ring my bell~
   var req_body = req.body["m2m:sgn"].nev.rep["m2m:cin"];
   if(req_body != undefined){
      console.log("\n[NOTIFICATION]")
      console.log(req_body);
      var content = req_body.con; 
      //이건 진동벨 번호! 이건 받아오긴 하는데
      //여기서 임의로 부여해야 할지는 생각해 봐야 할 문제인듯
      //안에 내용 작성 필요!!!
      res.sendStatus(200);
   }
});
app.post("/pos", function(req, res){ //pos기
   var req_body = req.body["m2m:sgn"].nev.rep["m2m:cin"];
   if(req_body != undefined){
      console.log("\n[NOTIFICATION]")
      console.log(req_body);
      var content = req_body.con; //주문번호 등록 필요!
      //안에 내용 작성 필요!!!
      res.sendStatus(200);
   }
});
*/
/*
app.post("/"+aeId, function (req, res) {
   var req_body = req.body["m2m:sgn"].nev.rep["m2m:cin"];
   if(req_body != undefined) {
      console.log("\n[NOTIFICATION]")
      console.log(req_body);
      var content = req_body.con;
      console.log("Receieved luminosity: " + content);
      if (content > threshold && cnt_value == 1) {
         console.log("High luminosity => Switch lamp to 0");
         for (var i = 0; i < arr_container.length; i++) {
            createContenInstance(i, "0");
         }
         cnt_value = 0;
      } else if (content < threshold && cnt_value == 0) {
         console.log("Low luminosity => Switch lamp to 1");
         for (var i = 0; i < arr_container.length; i++) {
            createContenInstance(i, "1");
         }
         cnt_value = 1;
      } else {
         console.log("Nothing to do");
      }
      res.sendStatus(200);
   }
});
*/

function GetCin(URL, AE, CNT){
   console.log("--------get cin-------");
   var method = "GET";
   var url= URL + "/Mobius/" + AE + "/" + CNT+'/la';
   // CSE/AE/CNT/CIN 
   // 이거 cnt 뒤에 tim 검사해야 함
   var requestId = Math.floor(Math.random() * 10000);
   console.log(url+'\n');
   var options = {
      url: url,
      method: method,
      headers: {
         "Accept": "application/json",
         "X-M2M-Origin": 'S',
         "X-M2M-RI": requestId, //random 생성
      }
   };

   request(options, function (error, response, body) {
      console.log("--------get cin [RESPONSE]--------");
      if(error){
         console.log(error);
      }else{
         console.log(response.statusCode);
         var res_json = JSON.parse(body);
         var cin_con=(res_json["m2m:cin"]["con"]);
         console.log(cin_con);
         return cin_con;
      }
   });
}

//각각의 ae 생성하는 function의 기능
createAE();
function createAE(){
   /*
   GetCin("http://10.0.1.19:7579","menu","Americano");
   GetCin("http://10.0.1.19:7579","menu","Latte");
   GetCin("http://10.0.1.19:7579","menu","Tea");
   GetCin("http://10.0.1.19:7579","control","JoyChar");
   */
   console.log("-------create AE-------");
   var method = "POST";
   var url= cseUrl+"/Mobius";
   var resourceType=2;
   var requestId = Math.floor(Math.random() * 10000);
   var representation = {
      "m2m:ae":{
         "rn":aeName,         
         "api":"app.company.com",
         "rr":"true",
         "poa":["http://"+aeIp+":"+aePort]
      }
   };

   console.log(method+" "+url);
   console.log(representation);

   var options = {
      url: url,
      method: method,
      headers: {
         "Accept": "application/json",
         "X-M2M-Origin": aeId,
         "X-M2M-RI": requestId,
         "Content-Type": "application/json;ty="+resourceType
      },
      json: representation
   };

   request(options, function (error, response, body) {
      console.log("-------create AE [REQUEST]-------");
      if(error){
         console.log(error);
      }else{
         console.log(response.statusCode);
         console.log(body);
         if(response.statusCode==409){
            resetAE();
         }else{
            //createSubscription(AE, CNT);
            createSubscription('menu','menu'); //CNT 추가
            createSubscription('control','JoyChar');
         }
      }
   });
}


function resetAE(){
   console.log("\n-------reset AE-------");
   var method = "DELETE";
   var url= cseUrl+"/Mobius/"+aeName;
   var requestId = Math.floor(Math.random() * 10000);

   console.log(method+" "+url);

   var options = {
      url: url,
      method: method,
      headers: {
         "Accept": "application/json",
         "X-M2M-Origin": aeId,
         "X-M2M-RI": requestId,
      }
   };

   request(options, function (error, response, body) {
      console.log("\n-------reset AE [REQUEST]-------");
      if(error){
         console.log(error);
      }else{         
         console.log(response.statusCode); //숫자 코드
         console.log(body); //postman body 부분
         createAE();
      }
   });
}

function resetSub(AE, CNT){
   console.log("\n-------reset subscription-------");
   var method = "DELETE";
   var requestId = Math.floor(Math.random() * 10000);
   //var url= cseUrl+sub_Container+'/sub';
   var url = cseUrl+"/Mobius/"+AE+'/'+CNT+'/sub';
   //이거 구독부분임
   var options = {
      url: url,
      method: method,
      headers: {
         "Accept": "application/json",
         "X-M2M-Origin": aeId,
         "X-M2M-RI": requestId,
      }
   };
   console.log("-------------------------"); 
   console.log(url);
   console.log("-------------------------");
   request(options, function (error, response, body) {
      console.log("-------resetsub [REQUEST]-------");
      if(error){
         console.log(error);
      }else{
         console.log(response.statusCode);
         console.log(body);
         createSubscription(AE, CNT);
      }
   });
}

function createSubscription(AE, CNT){
   //구독 걸어주는 부분인거 같음
   console.log("\n-------create subscription-------");
   var method = "POST";
   //var url= cseUrl+sub_Container;
   var url = cseUrl+"/Mobius/"+AE+"/"+CNT;
   console.log("---------------------------");
   console.log(CNT);
   console.log("---------------------------");
   // url = cseUrl/Mobius/menu/Americano
   // url = cseUrl/Mobius/menu/Latte
   // url = cseUrl/Mobius/menu/Tea
   var resourceType=23;
   var requestId = Math.floor(Math.random() * 10000);
   var representation = {
      "m2m:sub": {
         "rn": "sub",
         "nu": ["http://"+aeIp+":"+aePort+"/"+AE+"?ct=json"],
         "nct": 2,
         "enc": {
            "net": [3]
         }
      }
   };
   console.log(method+" "+url); //POST
   console.log(representation); 

   var options = {
      url: url,
      method: method,
      headers:{
         "Accept": "application/json",
         "X-M2M-Origin": aeId,
         "X-M2M-RI": requestId,
         "Content-Type": "application/json;ty="+resourceType
      },
      json: representation
   };
   console.log(options.headers);
   //3개를 request로 전송 후 response를 한꺼번에 받음
   request(options, function (error, response, body) {
      console.log("-------create sub [REQUEST]-------");
      if(error){
         console.log(error);
      }else{
         console.log(response.statusCode);
         console.log(body); //postman body
         if(response.statusCode==409){
            resetSub(AE, CNT);
         }
      }

   });
}

function createContenInstance(AEURL, AE, CNT, value){
   if(arr_container.length == 0){
      console.log("No more paths to create")
   }else{
      console.log("\n-------create cin-------");
      var method = "POST";
      //var url= cseUrl+arr_container[count].path;
      var url = AEURL+"/Mobius/"+AE+"/"+CNT;
      // url = cseUrl/Mobius/menu/Americano
      // url = cseUrl/Mobius/menu/Latte
      // url = cseUrl/Mobius/menu/Tea
      var resourceType=4;
      var requestId = Math.floor(Math.random() * 10000);
      var representation = {
         "m2m:cin":{
               "con": value
            }
         };

      console.log(method+" "+url);
      console.log(representation);

      var options = {
         url: url,
         method: method,
         headers: {
            "Accept": "application/json",
            "X-M2M-Origin": aeId,
            "X-M2M-RI": requestId,
            "Content-Type": "application/json;ty="+resourceType
         },
         json: representation
      };

      request(options, function (error, response, body) {
         console.log("-------create cin [REQUEST]-------");
         if(error){
            console.log(error);
         }else{
            console.log(response.statusCode);
            console.log(body);
         }
      });
   }
}