#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Servo.h>

// Update these with values suitable for your network.

const char* ssid = "InfoSec";
const char* password = "wjdqhqhgh16";
const char* mqtt_server = "10.0.1.26";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (500)
char msg[MSG_BUFFER_SIZE];
String packet;
char JoyChar;
int joyState = 0;
char menu[3] = {'A', 'L', 'T'};
int m_index = 0;
char Joy;
int j_int = 0;
int t_int = 0;
int fin = 0;
int W = D0;

#define joystick_x A0
#define joystick_y D1
#define joystick_sw D2

const int servoPin1 = D5;
const int servoPin2 = D6;

int SW_Pin = D4;
int sensor = D8;

Servo servo1;
Servo servo2;

int check_joyState() {
  joyState = digitalRead(joystick_sw);

  if(joyState == HIGH) {
    return 1;
  }
  else {
    return 0;
  }
}

void ControlJoy() {
  int swValue = analogRead(joystick_sw);
  int xValue = analogRead(joystick_x);
  int yValue = digitalRead(joystick_y);
  
  while(check_joyState()) {
    delay(100);
    Serial.print(".");
  }

  if(xValue > 600 and xValue < 900 and yValue == 0) {
    JoyChar = 'L';
    j_int = 1;
    m_index -= 1;
    if(m_index == -1) m_index = 2;
  }
  else if(xValue > 600 and xValue < 900 and yValue == 1) {
    JoyChar = 'R';
    j_int = 2;
    m_index += 1;
    if(m_index == 3) m_index = 0;
  }
  else if(xValue > 1000 and yValue == LOW) {
    JoyChar = 'U';
    j_int = 3;
    fin = 0;
  }
  else if (xValue < 200 and yValue == HIGH) {
    JoyChar = 'D';
    j_int = 4;
    fin = 4;
  }
  
  if(!espClient.connect("10.0.1.26", 7579)) {
    Serial.println("connection failed");
  }
  else {
    String content = "{\"m2m:cin\":{\"con\":\"";
    content.concat(j_int);
    content.concat("\"}}");

    String doc = "POST /Mobius/control/JoyChar HTTP/1.1\r\nX-M2M-Origin: S\r\nX-M2M-RI: req30\r\nContent-Type: application/json;ty=4\r\nhost: 10.0.1.26:7579 \r\naccept: application/json\r\ncontent-length: ";
    doc.concat(content.length());
    doc.concat("\r\nConnection: close\r\n\r\n");
    doc.concat(content);
    
    Serial.print("Json data : ");
    Serial.println(JoyChar);
  
    doc.toCharArray(msg,500);
    Serial.println(msg);
    espClient.write(msg); 
    String recevbline = espClient.readStringUntil('\r');
    Serial.println(recevbline);

  }
}

void ControlPad(char menuu) {
  if(menuu == 'A') {
    Serial.print(menuu);
    servo1.attach(servoPin1);
  }
  else if(menuu == 'L') {
    Serial.print(menuu);
    servo2.attach(servoPin2);
  }
  else if(menuu == 'T') {
    Serial.print(menuu);
    servo1.attach(servoPin1);
    servo2.attach(servoPin2);
  }  

  servo1.write(90);
  servo2.write(90);
  delay(500);
  servo1.write(0);
  servo2.write(0);
  delay(500);
  servo1.detach();
  servo2.detach();
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void ControlSwitch(char menuu) {
  if(menuu == 'A') t_int = 1;
  else if(menuu == 'L') t_int = 2;
  else if(menuu == 'T') t_int = 3;

  if(!espClient.connect("10.0.1.26", 7579)) {
    Serial.println("connection failed");
  }
  else {
    String content = "{\"m2m:cin\":{\"con\":\"";
    if(fin == 0) {
      content.concat(t_int);
    }
    else {
      content.concat(fin);
    }
    content.concat("\"}}");

    String doc = "POST /Mobius/menu/menu HTTP/1.1\r\nX-M2M-Origin: S\r\nX-M2M-RI: req30\r\nContent-Type: application/json;ty=4\r\nhost: 10.0.1.26:7579 \r\naccept: application/json\r\ncontent-length: ";
    doc.concat(content.length());
    doc.concat("\r\nConnection: close\r\n\r\n");
    doc.concat(content);
  
    doc.toCharArray(msg,500);
    Serial.println(msg);
    espClient.write(msg); 
    String recevbline = espClient.readStringUntil('\r');
    Serial.println(recevbline);
  }
}

void start_cosk() {
  if(!espClient.connect("10.0.1.26", 7579)) {
    Serial.println("connection failed");
  }
  else {
    String content = "{\"m2m:cin\":{\"con\":\"";
    content.concat(1);
    content.concat("\"}}");

    String doc = "POST /Mobius/control/sensor HTTP/1.1\r\nX-M2M-Origin: S\r\nX-M2M-RI: req30\r\nContent-Type: application/json;ty=4\r\nhost: 10.0.1.26:7579 \r\naccept: application/json\r\ncontent-length: ";
    doc.concat(content.length());
    doc.concat("\r\nConnection: close\r\n\r\n");
    doc.concat(content);
  
    doc.toCharArray(msg,500);
    Serial.println(msg);
    espClient.write(msg); 
    String recevbline = espClient.readStringUntil('\r');
    Serial.println(recevbline);
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(joystick_x, INPUT);
  pinMode(joystick_y, INPUT);
  pinMode(joystick_sw, INPUT_PULLUP);
  pinMode(SW_Pin, INPUT);
  pinMode(sensor, INPUT);
  digitalWrite(W, HIGH);
  pinMode(W, OUTPUT);
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  if(digitalRead(sensor) == HIGH) {
    start_cosk();
    ControlJoy();
    delay(500);
    ControlPad(menu[m_index]);
    delay(500);
    int BS = digitalRead(SW_Pin);
    if(BS == 0) {
      ControlSwitch(menu[m_index]);
    }
    if(fin == 4) {
      digitalWrite(W, LOW);
    }
    delay(100);
  }
}
