#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "InfoSec";
const char* password = "wjdqhqhgh16";
const char* mqtt_server = "/*너네꺼로 해라 ㅡㅡ*/";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
String packet;
char JoyChar;
int joyState = 0;

#define joystick_x D0
#define joystick_y D1
#define joystick_sw D2

int check_joyState() {
  joyState = digitalRead(joystick_sw);

  if(joyState == HIGH) {
    return 1;
  }
  else {
    return 0;
  }
}

char ControlJoy() {
  int swValue = analogRead(joystick_sw);
  int xValue = digitalRead(joystick_x);
  int yValue = digitalRead(joystick_y);

  while(check_joyState()) {
    delay(100);
    Serial.print(".");
  }
  
  Serial.print("  X : ");
  Serial.print(xValue);
  Serial.print("  Y : ");
  Serial.print(yValue);
  Serial.print("  SW : ");
  Serial.println(swValue);

  if(yValue == 0) JoyChar = 'L';
  else if(yValue == 1) JoyChar = 'R';
  

  return JoyChar;
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(joystick_x, INPUT);
  pinMode(joystick_y, INPUT);
  pinMode(joystick_sw, INPUT_PULLUP);
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void mqtt_publish(char JoyChar) {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    packet = "Joystick: " + String(JoyChar);
    packet.toCharArray(msg, 50);
    
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("outTopic", msg);
  }
}

void loop() {
  JoyChar = ControlJoy();
  
  mqtt_publish(JoyChar);
}
