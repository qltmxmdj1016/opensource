#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "InfoSec";
const char* password = "wjdqhqhgh16";
const char* mqtt_server = "10.0.1.26";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (1000)
char msg[MSG_BUFFER_SIZE];
char gotmsg[MSG_BUFFER_SIZE];
int value = 0;
int S = D2;
int W = D7;
int table = 4;
int tact = D4;
String cnt = "0";

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void send_info() {
  if(!espClient.connect("10.0.1.26", 7579)) {
    Serial.println("connection failed");
  }
  else {
    String content = "{\"m2m:cin\":{\"con\":\"";
    content.concat(cnt.toInt());
    content.concat(",");
    content.concat(table);
    content.concat("\"}}");

    String doc = "POST /Mobius/Bill/info HTTP/1.1\r\nX-M2M-Origin: S\r\nX-M2M-RI: req30\r\nContent-Type: application/json;ty=4\r\nhost: 10.0.1.26:7579 \r\naccept: application/json\r\ncontent-length: ";
    doc.concat(content.length());
    doc.concat("\r\nConnection: close\r\n\r\n");
    doc.concat(content);

    doc.toCharArray(msg, 500);
    Serial.println(msg);
    espClient.write(msg);
    String recevbline = espClient.readStringUntil('\r');
    Serial.println(recevbline);
  }
}

void get_info() {
  int j = 6;
  Serial.println(gotmsg);
  while(j < 1000) {
    if(gotmsg[j-6] == 'c' and gotmsg[j-5] == 'o' and gotmsg[j-4] == 'n') {
      cnt = gotmsg[j];
      break;  
    }
    j++;
  }
  Serial.println(cnt.toInt());
}

void setup() {
  Serial.begin(115200);
  pinMode(tact, INPUT);
  digitalWrite(W, HIGH);
  delay(1000);
  pinMode(W, OUTPUT);
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  int BS = digitalRead(tact);
  Serial.println(BS);
  if(BS == HIGH) {
    delay(5000);
    digitalWrite(W, LOW);
  }
  else {
    if(espClient.connect("10.0.1.18", 7579)) {
      espClient.println("GET /Mobius/Bill/info/latest HTTP/1.1\r\nX-M2M-Origin: S\r\nX-M2M-RI: 12345\r\nContent-Type: application/json;ty=3\r\nhost: 10.0.1.18:7579 \r\naccept: application/json");
      espClient.println("Host: 10.0.1.18:7579");
      espClient.println("Connection: close");
      espClient.println();
    }
  }
}

void loop() {
  int i = 0;
  while(cnt.toInt() == 0) {
    while(espClient.available()) {
      char c = espClient.read();
      gotmsg[i] += c;
      i++;
    }
  
    get_info();
    delay(3000);
  }

  int tmp = digitalRead(S);
  
  while(tmp == 1) {
    delay(1000);
    Serial.print(".");
    tmp = digitalRead(S);
  }

  send_info();
  digitalWrite(W, LOW);
}
